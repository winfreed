#!/usr/bin/env python
#
# winfreed - download a selection of Free Software for MS Windows.
#
# Copyright (C) 2011-2014  Antonio Ospite <ao2@ao2.it>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import sys
import glob
import ConfigParser
import urllib2
from progressbar import Bar, ETA, FileTransferSpeed, Percentage, ProgressBar

OUTPUT_DIR = 'downloads'
LANGCODE = 'en-US'

# One of i386 or x64
ARCHITECTURE = 'x64'

PKG_DIR = 'pkgs'
CHUNK_SIZE = 8192


def get_pkg(pkg_file):
    config = ConfigParser.SafeConfigParser({'language': LANGCODE})
    with open(pkg_file, mode='r') as f:
        config.readfp(f)
        pkg = dict(config.items('Package'))
        basename = os.path.basename(pkg_file)
        package_name = os.path.splitext(basename)[0]
        pkg['package_name'] = package_name
        f.close()
        return pkg

    return None


def process_all(path, cb):
    listing = sorted(glob.glob(os.path.join(path, '*.ini')))
    for pkg_file in listing:
        pkg = get_pkg(pkg_file)
        if not pkg:
            sys.stderr.write("Error: cannot get a pkg for: %s\n" % pkg_file)
            continue
        cb(pkg)


def show(pkg):
    print 'Package:  ', pkg['package_name']
    print 'Program:  ', pkg['name']
    if pkg.has_key('version'):
        print 'Version:  ', pkg['version']
    print 'Homepage: ', pkg['homepage']
    print


def download_file(src_url, dest_dir):
    try:
        hdr = {
            'User-Agent': 'Wget/1.13.4 (linux-gnu)',
        }
        request = urllib2.Request(src_url, headers=hdr)
        response = urllib2.urlopen(request)
    except Exception, e:
        print 'Downloading from', src_url, 'failed.', e
        return

    # get the final URL after possible redirect have been followed
    url = response.geturl()

    filename = ""

    # From http://paste.pound-python.org/show/9545/
    # TODO: use a proper module to parse HTTP headers
    if 'Content-Disposition' in response.info() and len(response.info()['Content-Disposition'].split('filename=')) > 1:
        # If the response has Content-Disposition, we take file name from it
        filename = response.info()['Content-Disposition'].split('filename=')[1].decode('utf-8')
        if filename[0] == '"' or filename[0] == "'":
            filename = urllib2.unquote(filename.split('"')[1])
    else:
        filename = urllib2.unquote(url.split('/')[-1].decode('utf_8'))

    if filename == "":
        sys.stderr.write("Debug (%s): filename: %s url: %s\n" % (pkg['package_name'], filename, url))
        return

    # TODO: Add some integrity verification of downloaded files (md5, sha256?)

    destfile = os.path.join(dest_dir, filename)
    if os.path.exists(destfile):
        # TODO: check if the file is a full download from previous run,
        # if not download again discarding the existing file?
        sys.stderr.write("Warning: %s exists!\n" % destfile)
        return

    if 'Content-Length' not in response.info():
        sys.stderr.write("Cannot get the size of the file at %s\n" % src_url)
        return

    total_size = response.info().getheader('Content-Length').strip()
    total_size = int(total_size)

    #widgets = [pkg['name'], ' ', Percentage(), ' ', Bar(marker='=', left='[', right=']'),
    widgets = [filename, ' ', Percentage(), ' ', Bar(marker='=', left='[', right=']'),
               ' ', ETA(), ' ', FileTransferSpeed()]
    pbar = ProgressBar(widgets=widgets, maxval=total_size).start()

    outfile = open(destfile, mode='w')

    bytes_so_far = 0
    while 1:
        chunk = response.read(CHUNK_SIZE)
        if not chunk:
            break

        bytes_so_far += len(chunk)
        outfile.write(chunk)
        pbar.update(bytes_so_far)
    pbar.finish()

    outfile.close()


def download(pkg):
    try:
        pkg_url = pkg['url_' + ARCHITECTURE]
    except KeyError:
        pkg_url = pkg['url']
    download_file(pkg_url, OUTPUT_DIR)


def show_all():
    process_all(PKG_DIR, show)


def download_all():
    if os.path.exists(OUTPUT_DIR) == False:
        os.mkdir(OUTPUT_DIR, 0755)

    process_all(PKG_DIR, download)


def usage():
    usage = "winfreed - download a selection of Free Software for MS Windows.\n\n"
    usage += "usage: %s <COMMAND>\n\n" % sys.argv[0]
    usage += "COMMANDS:\n"
    usage += "\tshow        Show info about all the available packages\n"
    usage += "\tdownload    Download all the packages\n"
    print usage

if __name__ == "__main__":

    if len(sys.argv) < 2:
        usage()
        sys.exit(1)

    if sys.argv[1] == 'download':
        if len(sys.argv) > 2:
            pkg = get_pkg(sys.argv[2])
            download(pkg)
        else:
            download_all()
    elif sys.argv[1] == 'show':
        show_all()
    else:
        usage()
        sys.exit(1)

    sys.exit(0)
